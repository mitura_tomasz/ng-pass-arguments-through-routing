import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './modules/profile/profile.component';
import { HomeComponent } from './modules/home/home.component';

import { StatsResolve } from './resolves/stats.resolve';


const appRoutes: Routes = [
  {path: '', component: HomeComponent, data: [{ word: 'hello' }]},
  {
    path: 'profile',
    component: ProfileComponent,
    resolve: {   
      stats: StatsResolve // The way to passing data through router 
    }
  },
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }