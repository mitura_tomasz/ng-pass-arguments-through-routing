import { TestBed, inject } from '@angular/core/testing';

import { StatsResolveService } from './stats-resolve.service';

describe('StatsResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StatsResolveService]
    });
  });

  it('should be created', inject([StatsResolveService], (service: StatsResolveService) => {
    expect(service).toBeTruthy();
  }));
});
