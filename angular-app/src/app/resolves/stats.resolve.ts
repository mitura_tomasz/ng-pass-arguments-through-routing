import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';


@Injectable()
export class StatsResolve implements Resolve<any> {

  stats: any[] = [
    {
      date: '02-05-2003',
      goals: '12'
    },
    {
      date: '02-05-2003',
      goals: '6'
    },
    {
      date: '02-05-2003',
      goals: '27'
    },
  ]
  const statsPromise: Promise<any[]> = Promise.resolve(this.stats);

  constructor() { }

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): any[] {
    return this.statsPromise.then(stats => stats);
  }
}