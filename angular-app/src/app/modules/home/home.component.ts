import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  word: any;
  
  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.word = this._activatedRoute.data.value[0].word;    
  }

}
