import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  
  users: any[];
  stats: any[];

  constructor(
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    
    this._activatedRoute.data.forEach(users => {
      this.users = users;
    });
    
    this._activatedRoute.data.forEach(statsData => {
      this.stats = statsData.stats;
    })
  }

}
