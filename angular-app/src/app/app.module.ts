import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { HomeModule } from './modules/home/home.module';
import { ProfileModule } from './modules/profile/profile.module';
import { NavbarComponent } from './components/navbar/navbar.component';

import { StatsResolve } from './resolves/stats.resolve';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    ProfileModule
  ],
  providers: [
    StatsResolve
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
